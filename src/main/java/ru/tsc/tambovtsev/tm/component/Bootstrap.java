package ru.tsc.tambovtsev.tm.component;

import ru.tsc.tambovtsev.tm.api.controller.ICommandController;
import ru.tsc.tambovtsev.tm.api.controller.IProjectController;
import ru.tsc.tambovtsev.tm.api.controller.IProjectTaskController;
import ru.tsc.tambovtsev.tm.api.controller.ITaskController;
import ru.tsc.tambovtsev.tm.api.repository.ICommandRepository;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.ICommandService;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.api.service.IProjectTaskService;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.constant.ArgumentConst;
import ru.tsc.tambovtsev.tm.constant.TerminalConst;
import ru.tsc.tambovtsev.tm.controller.CommandController;
import ru.tsc.tambovtsev.tm.controller.ProjectController;
import ru.tsc.tambovtsev.tm.controller.ProjectTaskController;
import ru.tsc.tambovtsev.tm.controller.TaskController;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.tambovtsev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.repository.CommandRepository;
import ru.tsc.tambovtsev.tm.repository.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.TaskRepository;
import ru.tsc.tambovtsev.tm.service.CommandService;
import ru.tsc.tambovtsev.tm.service.ProjectService;
import ru.tsc.tambovtsev.tm.service.ProjectTaskService;
import ru.tsc.tambovtsev.tm.service.TaskService;
import ru.tsc.tambovtsev.tm.util.DateUtil;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (processArgument(args)) System.exit(0);
        initData();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void initData() {
        taskService.create("DEMO TASK", "Simple task...");
        taskService.create("TEST TASK", "Simple task...");
        taskService.create("MEGA TASK", "Simple task...");
        projectService.add(new Project("DEMO PROJECT", Status.NOT_STARTED, DateUtil.toDate("04.10.2019")));
        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("05.03.2018")));
        projectService.add(new Project("MEGA PROJECT", Status.IN_PROGRESS, DateUtil.toDate("16.02.2020")));
        projectService.add(new Project("BEST PROJECT", Status.COMPLETED, DateUtil.toDate("22.01.2021")));
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        switch (command) {
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_TO_PROJECT:
                projectTaskController.unbindTaskToProject();
                break;
            case TerminalConst.TASK_LIST_SHOW_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException();
        switch (arg) {
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    public void close() {
        System.exit(0);
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}