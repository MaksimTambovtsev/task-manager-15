package ru.tsc.tambovtsev.tm.exception.system;

import ru.tsc.tambovtsev.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command ``" + command + "`` is not supported...");
    }
}
